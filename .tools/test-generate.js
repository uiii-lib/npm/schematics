const {spawn} = require("child_process");

const [schematicsName, ...args] = process.argv.slice(2)

if (!schematicsName) {
	console.log(`Usage: node generate.js <schematic> [..args]`)
	console.log("args ... schematic args except 'app path'")
	process.exit(1);
}

spawn(
	'node',
	[
		require.resolve('@angular-devkit/schematics-cli/bin/schematics.js', {paths: module.paths}),
		`.:${schematicsName}`,
		'--debug=false',
		'.test-output',
		...args
	],
	{
		stdio: 'inherit',
		shell: true
	}
);
