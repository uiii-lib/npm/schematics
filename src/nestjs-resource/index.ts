import { join, strings, Path } from '@angular-devkit/core';
import { mergeWith, Rule, SchematicContext, Tree, schematic, noop } from '@angular-devkit/schematics';

import { Schematic, SchematicOptions } from '../helpers/schematic';

export interface NestjsResourceSchematicOptions {
	moduleName: string;
	resourceName: string;
	createResolver: boolean;
	createRepositoryType: string;
}

export class NestjsResourceSchematic extends Schematic<NestjsResourceSchematicOptions> {
	public exists(tree: Tree, context: SchematicContext): boolean {
		return tree.exists(join(this.options.path, this.options.moduleName, 'model', `${this.options.resourceName}.ts`));
	}

	protected prepareOptions(options: SchematicOptions<NestjsResourceSchematicOptions>): SchematicOptions<NestjsResourceSchematicOptions> {
		options.moduleName = strings.dasherize(options.moduleName);
		options.resourceName = options.resourceName ? strings.dasherize(options.resourceName) : options.moduleName;

		return options;
	}

	protected buildRules(tree: Tree, context: SchematicContext): Rule[] {
		return [
			schematic('nestjs-module', {...this.options, skipExisting: true}, {interactive: false}),
			mergeWith(this.files(tree)),
			this.options.createRepositoryType !== 'none'
				? schematic(`nestjs-repository-${this.options.createRepositoryType}`, this.options, {interactive: false})
				: noop(),
			this.options.createResolver
				? schematic('nestjs-resolver', this.options, {interactive: false})
				: noop()
		];
	}
}

export function main(options: NestjsResourceSchematicOptions): Rule {
	const schematic = new NestjsResourceSchematic(options);
	return schematic.build();
}
