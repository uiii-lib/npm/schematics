import { join, strings, Path } from '@angular-devkit/core';
import { Rule, SchematicContext, Tree, schematic, mergeWith } from '@angular-devkit/schematics';

import { addImportDeclaration } from '../helpers/add-import-declaration';
import { addModuleMetadataDeclaration } from '../helpers/add-module-metadata-declaration';
import { Schematic, SchematicOptions } from '../helpers/schematic';

export interface NestjsResolverSchematicOptions {
	moduleName: string;
	configName: string;
}

export class NestjsResolverSchematic extends Schematic<NestjsResolverSchematicOptions> {
	public exists(tree: Tree, context: SchematicContext): boolean {
		return tree.exists(join(this.options.path, this.options.moduleName, `${this.options.configName}.config.ts`));
	}

	protected prepareOptions(options: SchematicOptions<NestjsResolverSchematicOptions>): SchematicOptions<NestjsResolverSchematicOptions> {
		options.moduleName = strings.dasherize(options.moduleName);
		options.configName = options.configName ? strings.dasherize(options.configName) : options.moduleName;

		return options;
	}

	protected buildRules(tree: Tree, context: SchematicContext): Rule[] {
		const moduleFilePath = join(this.options.path, this.options.moduleName, `${this.options.moduleName}.module.ts`);

		return [
			schematic('nestjs-module', {...this.options, skipExisting: true}, {interactive: false}),
			addImportDeclaration(moduleFilePath, `${strings.classify(this.options.configName)}Config`, `./${this.options.configName}.config`),
			addImportDeclaration(moduleFilePath, 'ConfigModule', '@uiii-lib/nestjs-config'),
			addModuleMetadataDeclaration(moduleFilePath, `${strings.classify(this.options.moduleName)}Module`, 'imports', 'ConfigModule'),
			addModuleMetadataDeclaration(moduleFilePath, `${strings.classify(this.options.moduleName)}Module`, 'providers', `${strings.classify(this.options.configName)}Config`),
			mergeWith(this.files(tree))
		]
	}
}

export function main(options: NestjsResolverSchematicOptions): Rule {
	const schematic = new NestjsResolverSchematic(options);
	return schematic.build();
}
