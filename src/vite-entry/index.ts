import { join, normalize, strings } from '@angular-devkit/core';
import { Rule, SchematicContext, Tree, mergeWith } from '@angular-devkit/schematics';
import { NodePackageInstallTask } from '@angular-devkit/schematics/tasks';

import { Schematic, SchematicOptions } from '../helpers/schematic';
import path from 'path';

export interface ViteEntrySchematicOptions {
}

export class ViteEntrySchematic extends Schematic<ViteEntrySchematicOptions> {
	protected prepareOptions(options: SchematicOptions<ViteEntrySchematicOptions>) {
		options.path = join(
			normalize(strings.dasherize(options.appRootPath)),
			'src'
		);

		return options;
	}

	protected buildRules(tree: Tree, context: SchematicContext): Rule[] {
		return [
			(tree: Tree, context: SchematicContext) => {
				context.logger.info(`Installing react-router-dom as dependency`);
				context.logger.info(path.join(process.cwd(), this.options.appRootPath));
				context.addTask(new NodePackageInstallTask({
					packageName: 'react-router-dom',
					workingDirectory: this.options.appRootPath//path.join(process.cwd(), this.options.appRootPath)
				}));
				return tree;
			},
			mergeWith(this.files(tree))
		]
	}
}

export function main(options: ViteEntrySchematicOptions) {
	const schematic = new ViteEntrySchematic(options);
	return schematic.build();
}
