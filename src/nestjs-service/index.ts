import { join, strings } from '@angular-devkit/core';
import { Rule, SchematicContext, Tree, schematic, mergeWith } from '@angular-devkit/schematics';

import { addImportDeclaration } from '../helpers/add-import-declaration';
import { addModuleMetadataDeclaration } from '../helpers/add-module-metadata-declaration';
import { Schematic, SchematicOptions } from '../helpers/schematic';

export interface NestjsServiceSchematicOptions {
	moduleName: string;
	serviceName: string;
}

export class NestjsServiceSchematic extends Schematic<NestjsServiceSchematicOptions> {
	public exists(tree: Tree, context: SchematicContext): boolean {
		return tree.exists(join(this.options.path, this.options.moduleName, `${this.options.serviceName}.service.ts`));
	}

	protected prepareOptions(options: SchematicOptions<NestjsServiceSchematicOptions>): SchematicOptions<NestjsServiceSchematicOptions> {
		options.moduleName = strings.dasherize(options.moduleName);
		options.serviceName = options.serviceName ? strings.dasherize(options.serviceName) : options.moduleName;

		return options;
	}

	protected buildRules(tree: Tree, context: SchematicContext): Rule[] {
		const moduleFilePath = join(this.options.path, this.options.moduleName, `${this.options.moduleName}.module.ts`);

		return [
			schematic('nestjs-module', {...this.options, skipExisting: true}, {interactive: false}),
			addImportDeclaration(moduleFilePath, `${strings.classify(this.options.serviceName)}Service`, `./${this.options.serviceName}.service`),
			addModuleMetadataDeclaration(moduleFilePath, `${strings.classify(this.options.moduleName)}Module`, 'providers', `${strings.classify(this.options.serviceName)}Service`),
			mergeWith(this.files(tree))
		]
	}
}

export function main(options: NestjsServiceSchematicOptions): Rule {
	const schematic = new NestjsServiceSchematic(options);
	return schematic.build();
}
