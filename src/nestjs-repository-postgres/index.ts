import { strings } from '@angular-devkit/core';
import { Rule } from '@angular-devkit/schematics';

import { SchematicOptions } from '../helpers/schematic';
import { NestjsCustomRepositorySchematic, NestjsCustomRepositorySchematicOptions } from '../nestjs-repository-custom';

export interface NestjsPostgresRepositorySchematicOptions extends NestjsCustomRepositorySchematicOptions {
	tableName: string;
}

export class NestjsPostgresRepositorySchematic extends NestjsCustomRepositorySchematic<NestjsPostgresRepositorySchematicOptions> {
	protected prepareOptions(options: SchematicOptions<NestjsPostgresRepositorySchematicOptions>): SchematicOptions<NestjsPostgresRepositorySchematicOptions> {
		options = super.prepareOptions(options);
		options.tableName = options.tableName || `public.${strings.underscore(options.resourceName)}`;

		return options;
	}
}

export function main(options: NestjsPostgresRepositorySchematicOptions): Rule {
	const schematic = new NestjsPostgresRepositorySchematic(options);
	return schematic.build();
}
