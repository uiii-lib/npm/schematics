import { join, strings } from '@angular-devkit/core';
import { Rule, SchematicContext, Tree, schematic, mergeWith } from '@angular-devkit/schematics';

import { addImportDeclaration } from '../helpers/add-import-declaration';
import { addModuleMetadataDeclaration } from '../helpers/add-module-metadata-declaration';
import { Schematic, SchematicOptions } from '../helpers/schematic';
import { NestjsResourceSchematic } from '../nestjs-resource';

export interface NestjsCustomRepositorySchematicOptions {
	moduleName: string;
	resourceName: string;
}

export class NestjsCustomRepositorySchematic<O extends NestjsCustomRepositorySchematicOptions> extends Schematic<O> {
	public exists(tree: Tree, context: SchematicContext): boolean {
		return tree.exists(join(this.options.path, this.options.moduleName, `${this.options.resourceName}.repository.ts`));
	}

	protected prepareOptions(options: SchematicOptions<O>): SchematicOptions<O> {
		options.moduleName = strings.dasherize(options.moduleName);
		options.resourceName = options.resourceName ? strings.dasherize(options.resourceName) : options.moduleName;

		return options;
	}

	protected buildRules(tree: Tree, context: SchematicContext): Rule[] {
		const moduleFilePath = join(this.options.path, this.options.moduleName, `${this.options.moduleName}.module.ts`);

		const resourceSchematics = new NestjsResourceSchematic(this.options);
		if (!resourceSchematics.exists(tree, context)) {
			throw new Error(`Resource '${this.options.resourceName}' doesn't exist`);
		}

		return [
			schematic('nestjs-module', {...this.options, skipExisting: true}, {interactive: false}),
			addImportDeclaration(moduleFilePath, `${strings.classify(this.options.resourceName)}Repository`, `./${this.options.resourceName}.repository`),
			addModuleMetadataDeclaration(moduleFilePath, `${strings.classify(this.options.moduleName)}Module`, 'providers', `${strings.classify(this.options.resourceName)}Repository`),
			addModuleMetadataDeclaration(moduleFilePath, `${strings.classify(this.options.moduleName)}Module`, 'exports', `${strings.classify(this.options.resourceName)}Repository`),
			mergeWith(this.files(tree))
		]
	}
}

export function main(options: NestjsCustomRepositorySchematicOptions): Rule {
	const schematic = new NestjsCustomRepositorySchematic(options);
	return schematic.build();
}
