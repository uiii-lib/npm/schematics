import { join, strings, Path } from '@angular-devkit/core';
import { Rule, SchematicContext, Tree, schematic, mergeWith } from '@angular-devkit/schematics';

import { addImportDeclaration } from '../helpers/add-import-declaration';
import { addModuleMetadataDeclaration } from '../helpers/add-module-metadata-declaration';
import { Schematic, SchematicOptions } from '../helpers/schematic';

import { NestjsCustomRepositorySchematic } from '../nestjs-repository-custom';
import { NestjsResourceSchematic } from '../nestjs-resource';

export interface NestjsResolverSchematicOptions {
	moduleName: string;
	resourceName: string;
}

export class NestjsResolverSchematic extends Schematic<NestjsResolverSchematicOptions> {
	public exists(tree: Tree, context: SchematicContext): boolean {
		return tree.exists(join(this.options.path, this.options.moduleName, `${this.options.resourceName}.resolver.ts`));
	}

	protected prepareOptions(options: SchematicOptions<NestjsResolverSchematicOptions>): SchematicOptions<NestjsResolverSchematicOptions> {
		options.moduleName = strings.dasherize(options.moduleName);
		options.resourceName = options.resourceName ? strings.dasherize(options.resourceName) : options.moduleName;

		return options;
	}

	protected buildRules(tree: Tree, context: SchematicContext): Rule[] {
		const moduleFilePath = join(this.options.path, this.options.moduleName, `${this.options.moduleName}.module.ts`);

		const resourceSchematic = new NestjsResourceSchematic(this.options);
		if (!resourceSchematic.exists(tree, context)) {
			throw new Error(`Resource '${this.options.resourceName}' doesn't exist`);
		}

		return [
			schematic('nestjs-module', {...this.options, skipExisting: true}, {interactive: false}),
			addImportDeclaration(moduleFilePath, `${strings.classify(this.options.resourceName)}Resolver`, `./${this.options.resourceName}.resolver`),
			addModuleMetadataDeclaration(moduleFilePath, `${strings.classify(this.options.moduleName)}Module`, 'providers', `${strings.classify(this.options.resourceName)}Resolver`),
			mergeWith(this.files(tree))
		]
	}

	protected templateOptions(tree: Tree, context: SchematicContext): {} {
		const repositorySchematic = new NestjsCustomRepositorySchematic(this.options);

		return {
			repositoryExists: repositorySchematic.exists(tree, context)
		}
	}
}

export function main(options: NestjsResolverSchematicOptions): Rule {
	const schematic = new NestjsResolverSchematic(options);
	return schematic.build();
}
