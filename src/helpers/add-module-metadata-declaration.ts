import { SchematicsException, Tree } from '@angular-devkit/schematics';
import { tsquery } from '@phenomnomnominal/tsquery';
import ts from 'typescript';

import { showTsAST } from './show-ts-ast';

export const addModuleMetadataDeclaration = (filepath: string, moduleClass: string, metaProperty: string, declaration: string) => {
	return (tree: Tree) => {
		const text = tree.read(filepath);
		if (!text) throw new SchematicsException(`File ${filepath} doesn't exist.`);

		const source = text?.toString('utf-8');
		const sourceFile = ts.createSourceFile(filepath, source, ts.ScriptTarget.Latest, true);

		//showTsAST(sourceFile);

		const moduleClassNode = tsquery(sourceFile, "ClassDeclaration:has(Identifier[name=Module])")[0];

		if (!moduleClassNode) {
			console.log("node module class");
			return; // TODO
		}

		const arrayNode = tsquery(moduleClassNode, `Decorator > CallExpression:has(Identifier[name=Module]) PropertyAssignment:has(Identifier[name=${metaProperty}]) > ArrayLiteralExpression`)[0];
		const listNode = arrayNode?.getChildren().find(n => n.kind === ts.SyntaxKind.SyntaxList);

		if (!listNode) {
			console.log("no list node");
			return; // TODO
		}

		const listContent = listNode.getText().trim();

		if (listContent.replace(/\s/g, '').match(new RegExp(`(^|,)${declaration.replace(/\s/g, '')}($|,)`))) {
			// already in the list
			return;
		}

		const update = tree.beginUpdate(filepath);
		update.insertLeft(listNode.getEnd(), `${(listContent.length === 0 || listContent.endsWith(',')) ? '' : ','}\n\t\t${declaration.replace(/\n/g, '\t\t\n')}${listContent ? '' : '\n\t'}`);
		tree.commitUpdate(update);

		return tree
	};
}
