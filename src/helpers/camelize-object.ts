import { camelize } from "@angular-devkit/core/src/utils/strings";

export function camelizeObject(options: any) {
	options = Object.assign({}, options);

	for (let key of Object.keys(options)) {
		options[camelize(key)] = options[key];
	}

	return options;
}
