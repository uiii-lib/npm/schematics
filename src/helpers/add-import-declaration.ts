import { SchematicsException, Tree } from "@angular-devkit/schematics";
import { tsquery } from "@phenomnomnominal/tsquery";
import ts from "typescript";

export const addImportDeclaration = (filepath: string, what: string, from: string) => {
	return (tree: Tree) => {
		const text = tree.read(filepath);
		if (!text) throw new SchematicsException(`File ${filepath} doesn't exist.`);

		const source = text?.toString('utf-8');
		console.log(source);
		const sourceFile = ts.createSourceFile(filepath, source, ts.ScriptTarget.Latest, true);

		const importNodes = tsquery(sourceFile, "ImportDeclaration")!;

		let position = 0;
		let code = `import { ${what} } from '${from}';`;

		if (importNodes.find(it => it.getText() === code)) {
			return;
		}

		const lastSimilarImportNode = [...importNodes].reverse().find(it => {
			const importFrom = tsquery(it, "StringLiteral")[0]?.getText().replace(/['"]/g, '');

			if (from.startsWith('.')) {
				return importFrom.startsWith(from.match(/[.]+\//)![0]!);
			} else if (from.startsWith('@')) {
				return importFrom.startsWith('@')
			} else {
				return importFrom.match(/^[^.@]/);
			}
		});

		if (lastSimilarImportNode) {
			// add import to similar group
			position = lastSimilarImportNode.getEnd();
			code = `\n${code}`;
		} else if (importNodes.length > 0) {
			// add import to separate group
			console.log("ln", importNodes[importNodes.length - 1].getText());
			position = importNodes[importNodes.length - 1]!.getEnd();
			code = `\n\n${code}`;
		} else {
			// insert import in file beginning (first import)
			code = `${code}\n\n`;
		}

		const update = tree.beginUpdate(filepath);
		update.insertLeft(position, code);
		tree.commitUpdate(update);
	};
}
