import { join, normalize, Path, strings } from "@angular-devkit/core";
import { apply, chain, mergeWith, move, Rule, SchematicContext, Source, template, Tree } from "@angular-devkit/schematics";
import { rename } from "@angular-devkit/schematics/src/rules/rename";
import pluralize from "pluralize";

import { camelizeObject } from "./camelize-object";
import { files } from "./files.rule";

export type SchematicOptions<T> = T & {
	appRootPath: string;
	path: Path;
	skipExisting?: boolean;
}

export class Schematic<O> {
	options: SchematicOptions<O>;

	constructor(options: any) {
		options = camelizeObject(options);
		options.path = join(
			normalize(strings.dasherize(options.appRootPath)),
			'src'
		);

		this.options = this.prepareOptions(options);
	}

	public exists(tree: Tree, context: SchematicContext) {
		return false;
	}

	public build() {
		return (tree: Tree, context: SchematicContext) => {
			if (this.exists(tree, context) && this.options.skipExisting) {
				return;
			}

			return chain([
				...this.buildRules(tree, context)
			]);
		}
	}

	protected prepareOptions(options: SchematicOptions<O>) {
		return options;
	}

	protected buildRules(tree: Tree, context: SchematicContext): Rule[] {
		return [
			mergeWith(this.files(tree))
		];
	}

	protected templateOptions(tree: Tree, context: SchematicContext) {
		return {};
	}

	protected files(tree: Tree): Source {
		return (context: SchematicContext) => {
			return apply(files(context), [
				template({
					...strings,
					...this.options,
					plural: (name: string) => pluralize(name),
					...this.templateOptions(tree, context)
				}),
				rename(() => true, path => path.replace(/\.ejs$/, '')),
				move(this.options.path),
			])(context);
		}
	}
}
