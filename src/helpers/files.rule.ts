import { normalize, relative } from "@angular-devkit/core"
import { SchematicContext, url } from "@angular-devkit/schematics";

export const files = (context: SchematicContext) => {
	return url(`../../src/${context.schematic.description.name}/files`);
	//return relative(normalize(dirname), normalize(dirname.replace(/lib([\\/])/, 'src$1')));
}
