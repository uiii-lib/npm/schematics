import ts from "typescript";

export function showTsAST(node: ts.Node, level: number = 0): void {
	const indent = "  ".repeat(level);

	// will output the syntax kind of the node
	console.log(indent + ts.SyntaxKind[node.kind]);

	// output the text of node
	if (node.getChildCount() === 0) {
		console.log(indent + '\tText: ' + node.getText());
	}

	// output the children nodes
	node.forEachChild(child => showTsAST(child, level + 1));
}
