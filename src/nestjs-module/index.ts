import { join, normalize, strings } from '@angular-devkit/core';
import { SchematicContext, Tree } from '@angular-devkit/schematics';

import { Schematic, SchematicOptions } from '../helpers/schematic';

export interface NestjsModuleSchematicOptions {
	moduleName: string;
}

export class NestjsModuleSchematic extends Schematic<NestjsModuleSchematicOptions> {
	public exists(tree: Tree, context: SchematicContext) {
		const moduleFilePath = join(this.options.path, this.options.moduleName, `${this.options.moduleName}.module.ts`);
		return tree.exists(moduleFilePath);
	}

	protected prepareOptions(options: SchematicOptions<NestjsModuleSchematicOptions>) {
		options.moduleName = strings.dasherize(options.moduleName);
		options.path = join(
			normalize(strings.dasherize(options.appRootPath)),
			'src'
		);

		return options;
	}
}

export function main(options: NestjsModuleSchematicOptions) {
	const schematic = new NestjsModuleSchematic(options);
	return schematic.build();
}
