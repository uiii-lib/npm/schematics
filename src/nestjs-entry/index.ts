import { join, normalize, strings } from '@angular-devkit/core';
import { SchematicContext, Tree } from '@angular-devkit/schematics';

import { Schematic, SchematicOptions } from '../helpers/schematic';

export interface NestjsMainModuleSchematicOptions {
	entryName: string;
}

export class NestjsModuleSchematic extends Schematic<NestjsMainModuleSchematicOptions> {
	public exists(tree: Tree, context: SchematicContext) {
		const moduleFilePath = join(this.options.path, `${this.options.entryName}.ts`);
		return tree.exists(moduleFilePath);
	}

	protected prepareOptions(options: SchematicOptions<NestjsMainModuleSchematicOptions>) {
		options.entryName = strings.dasherize(options.entryName);
		options.path = join(
			normalize(strings.dasherize(options.appRootPath)),
			'src'
		);

		return options;
	}
}

export function main(options: NestjsMainModuleSchematicOptions) {
	const schematic = new NestjsModuleSchematic(options);
	return schematic.build();
}
